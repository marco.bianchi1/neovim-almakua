-- telescope keybindings
--local builtin = require('telescope.builtin') 
vim.keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<CR>");
vim.keymap.set("n", "<leader>fg", "<cmd>Telescope live_grep<CR>");
vim.keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<CR>")
