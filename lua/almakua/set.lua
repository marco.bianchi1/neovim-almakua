-- cursore ciccione in modalitá insert
vim.opt.guicursor = ""

-- attiva numeri a lato, e li mette relativi alla posizione
vim.opt.nu = true
vim.opt.relativenumber = true

-- set quanti spazi mette tab
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

-- disabilita il wrap oltre gli 80 caratteri
vim.opt.wrap = false

-- disabilitá i file di backup, ma tiene il file con la lista delle modifiche per avere una history totale
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- non attiva l'highlighting su tutte le parole
vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "80"
