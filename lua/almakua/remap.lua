-- set leader come spazio
vim.g.mapleader = " "
-- exit curent buffer and return to previous path
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

vim.keymap.set("n", "<leader><leader>", function()
    vim.cmd("so")
end)

-- :new open buffer
vim.keymap.set("n", "<leader>n", "<cmd>new<CR>");
-- :vnew open buffer vertically
vim.keymap.set("n", "<leader>nv", "<cmd>vnew<CR>");
-- split vertically
-- split
--
-- Ctrl-W V    Opens a new vertical split
-- Ctrl-W S    Opens a new horizontal split
-- Ctrl-W C    Closes a window
-- Ctrl-W O    Makes the current window the only one on screen and closes other windows
vim.keymap.set("n", "<leader>bp", "<cmd>bprev<CR>");
vim.keymap.set("n", "<leader>bn", "<cmd>bnext<CR>");

-- Disable q to start registering macro
vim.keymap.set("n", "Q", "<nop>")

