-- Safely require files
local function safe_require(module)
  local success, loaded_module = pcall(require, module)
  if success then
    return loaded_module
  end
  vim.cmd.echo('Error loading ' .. module)
end

safe_require('almakua.set')
safe_require('almakua.remap')
safe_require('almakua.bootstrap')
safe_require('almakua.telescope')
safe_require('almakua.catppuccin')
safe_require('almakua.harpoon')
safe_require('almakua.fugitive')
--require("almakua")
----require("plugins")
---- add config to run lazy as plugin manager 
--local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
--if not vim.loop.fs_stat(lazypath) then
--  vim.fn.system({
--    "git",
--    "clone",
--    "--filter=blob:none",
--    "https://github.com/folke/lazy.nvim.git",
--    "--branch=stable", -- latest stable release
--    lazypath,
--  })
--end
--vim.opt.rtp:prepend(lazypath)
--require("lazy").setup(
--    {
--        'nvim-telescope/telescope.nvim',
--        tag = '0.1.3',
--        dependencies = { 'nvim-lua/plenary.nvim' },
--        lazy = false
--    },
--    {
--        "catppuccin/nvim",
--        lazy = false
--    },
--    {
--        'nvim-treesitter/nvim-treesitter',
--        tag = false,
--        run = ':TSUpdate',
--        event="BufRead",
--        config = require('plugins.treesitter'),
--    }
--   
--)
--colorscheme catppuccin " catppuccin-latte, catppuccin-frappe, catppuccin-macchiato, catppuccin-mocha
--vim.cmd.colorscheme "catppuccin"
